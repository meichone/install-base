#!/bin/bash
clear
if [[ $EUID != 0 ]]; then
	figlet -c "Fail"
	echo -e "\tDebes ejecutar el script como usuario root"
	read -n 1 -p "apreta una tecla para salir."
	clear
else 
	rpm -qa | grep vsftpd >> /dev/null
	if [ $? == 1 ]; then
		yum install vsftpd ftp figlet -y
	       	mv /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.conf.bak
		touch /etc/vsftpd/vsftpd.conf
		echo anonymous_enable=NO >> /etc/vsftpd/vsftpd.conf
		echo local_enable=YES >> /etc/vsftpd/vsftpd.conf
		echo write_enable=YES >> /etc/vsftpd/vsftpd.conf
		echo local_umask=022 >> /etc/vsftpd/vsftpd.conf
		echo dirmessage_enable=YES >> /etc/vsftpd/vsftpd.conf
		echo xferlog_enable=YES >> /etc/vsftpd/vsftpd.conf
		echo connect_from_port_20=YES >> /etc/vsftpd/vsftpd.conf
		echo xferlog_std_format=YES >> /etc/vsftpd/vsftpd.conf
		echo chroot_local_user=YES >> /etc/vsftpd/vsftpd.conf
		echo chroot_list_enable=YES >> /etc/vsftpd/vsftpd.conf
		echo chroot_list_file=/etc/vsftpd/chroot_list >> /etc/vsftpd/vsftpd.conf
		echo listen=NO >> /etc/vsftpd/vsftpd.conf
		echo userlist_enable=YES >> /etc/vsftpd/vsftpd.conf
		echo pam_service_name=vsftpd >> /etc/vsftpd/vsftpd.conf
		echo ftpd_banner=BIENVENIDO SERVICIO FTP >> /etc/vsftpd/vsftpd.conf
		touch /etc/vsftpd/chroot_list
		systemctl restart vsftpd
	else
		while [ $? != 4 ];do
			figlet -c "FTP Install"
			echo -e "1)- Crear usuario FTP"
			echo -e "2)- Crear clave"
			echo -e "3)- Modificar usuaio"
			echo -e "4)- Salir Script"
			echo -e "Indica alternativa del 1 al 4: \c"
			read opc
			case $opc in
				1) echo -e "indica el nombre del usuario: \c"
				read usr
				cat /etc/passwd | grep -w $usr >> /dev/null
				if [ $? == 0 ]; then
					echo -e "El usuario ya existe en el sistema"
					exit
				else 
					useradd -d /var/ftp/$usr -s /sbin/nologin -g ftp $usr
					echo -e "El usuario $usr fue creado, indica la clave del usuario: \c"
					read -s clave
					echo "$clave" | passwd $usr --stdin &
					read -n 1 -p "El usuario fue creado y esta operativo, apreta una tecla para seguir"
					echo $usr >> /etc/vsftpd/chroot_list 
					clear
				fi
				;;
				2) echo -e "Indica el usuario a cambiar clave"
					read usr
					cat /etc/passwd | grep $usr >> /dev/null
					if [ $? == 0 ];then
						echo -e "Indica la Clave para $usr"
						read clave
						echo "$clave" | --stdin passwd $usr
					else
						echo -e "El usuario no existe"
						clear
						exit
					fi
					;;
				3) echo -e "\t Modificar"
					echo -e "Directorio Raiz"
					echo -e "Clave"
					echo -e "Eliminar usuario"
					clear
					;;
				4) read -n 1 -p "Apreta una tecla para salir"
				clear
				exit				
			esac
		done
	fi
fi




